const wrapper = document.querySelector('.wrapper')
const btn = document.querySelector('.found-button')
if (btn) {
    btn.addEventListener("click", findeMyIp);
}

async function findeMyIp(){
    const findeIp = await fetch('https://api.ipify.org/?format=json')
    const ip = await findeIp.json()
    const locationResponse = await fetch(`http://ip-api.com/json/${ip.ip}?fields=continent,country,region,city,district`)
    const location = await locationResponse.json()
    renderLocation(location.continent, location.country, location.region, location.city, location.district)
}

function renderLocation(continent, country, city, region, district){
    const locationList = document.createElement('ul')
    const continentPage = document.createElement('li')
    const districtPage = document.createElement('li')
    const countryPage = document.createElement('li')
    const regionPage = document.createElement('li')
    const cityPage = document.createElement('li')
    continentPage.innerText = continent
    districtPage.innerText = district
    countryPage.innerText = country
    regionPage.innerText = region
    cityPage.innerText = city
    wrapper.append(locationList)
    locationList.append(continentPage)
    locationList.append(districtPage)
    locationList.append(countryPage)
    locationList.append(regionPage)
    locationList.append(cityPage)
}